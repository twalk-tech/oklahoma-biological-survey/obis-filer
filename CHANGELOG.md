# Changelog

## 0.1.1 (2024-04-25)

### fixed (1 change)

- [ensuring the docker cmd points to an existing file](twalk-tech/oklahoma-biological-survey/obis-filer@b689faa9c4ed4a8ea2be97444785ae13a5d05e74) ([merge request](twalk-tech/oklahoma-biological-survey/obis-filer!6))

## 0.1.0 (2024-04-25)

### added (1 change)

- Initial implementation
