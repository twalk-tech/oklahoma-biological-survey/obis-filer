"""OBIS Filer application"""
from flask import request, jsonify
from utils import FileCreationException, get_flask_app, list_occurrence_files, save_occurrence_file

obis_filer = get_flask_app()

@obis_filer.route('/get-occurrence-files', methods=['GET'])
def get_occurrence_files():
  """Defines an API endpoint to get files for an occurrence record"""
  gid = request.args['gid']

  occurrence_files = list_occurrence_files(gid)

  if occurrence_files == []:
    return jsonify({'message': f'No files found for occurrence {gid}'})

  return jsonify({'message': occurrence_files}), 200

@obis_filer.route('/upload-occurrence-file', methods=['POST'])
def upload_occurrence_file():
  """Defines an API endpoint to upload a file for an occurrence record"""
  request_files = request.files

  if 'file' not in request_files:
    return jsonify({'message': 'No file part in the request'}), 400

  file = request_files['file']

  if file.filename == '':
    return jsonify({'message': 'No file selected'}), 400

  gid = request.form['gid']

  try:
    save_occurrence_file(gid, file.filename, file)
  except OSError as ose:
    return jsonify({'message': f'Error creating directory for occurrence {gid}: {str(ose)}'}), 500
  except FileCreationException as e:
    return jsonify({'message': f'Error uploading {file.filename} for occurrence {gid}: {str(e)}'}), 500

  return jsonify({'message': 'File uploaded successfully'}), 201

if __name__ == '__main__':
  from waitress import serve
  serve(obis_filer, host='0.0.0.0', port=3453)
