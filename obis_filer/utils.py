"""Defines utility functions for the OBIS Filer"""
import os

from flask import Flask

BASE_PATH = '/opt/obis-filer'

class FileCreationException(Exception):
  """Extends Exception class for errors when saving an uploaded file"""
  pass

def create_record_dir(dir_path: str):
  """Creates a directory for a record if it doesn't already exist"""
  if not os.path.exists(dir_path):
    try:
      os.makedirs(dir_path)
    except OSError as ose:
      raise ose

def list_files(dir_path: str) -> list:
  """List files in a directory for a record, or an empty list if there are no files"""
  if os.path.exists(dir_path):
    return os.listdir(dir_path)

  return []

# pylint: disable-next=fixme
# TODO: find a way to retrieve the date this file was uploaded
def list_occurrence_files(gid: str) -> list:
  """Returns a list of files for an occurrence record, if they exist"""
  gid_dir = os.path.join(BASE_PATH, 'occurrences', gid)

  return list_files(gid_dir)

# pylint: disable-next=fixme
# TODO: find a way to retain the user that uploaded the file, and the date of upload
def save_occurrence_file(gid: str, file_name: str, file):
  """Saves an uploaded file to an occurrence record's directory"""
  gid_dir = os.path.join(BASE_PATH, 'occurrences', gid)

  try:
    create_record_dir(gid_dir)
  except OSError as ose:
    raise ose

  file_path = f'{gid_dir}/{file_name}'

  if os.path.exists(file_path):
    raise FileCreationException('File already exists')

  try:
    file.save(file_path)
  except FileCreationException as e:
    raise e

def get_flask_app():
  """Returns a Flask application"""
  return Flask('obis_filer')
